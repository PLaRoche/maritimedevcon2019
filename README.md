
## Minimal Viable P(rocess)

Can be seen [here](https://plaroche.gitlab.io/maritimedevcon2019)
A talk given by [Patrick LaRoche](https://twitter.com/plaroche) at Martime Dev Conference, June 2019

## Links at end of deck


- My original blog on this topic
 [https://www.manifold.co/blog](https://www.manifold.co/blog)
- All awesome pics come from
 [https://unsplash.com/](https://unsplash.com/)
- My new home is looking for people [Lixar Careers](https://lixar.com/careers/) 

